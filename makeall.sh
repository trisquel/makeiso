#!/bin/bash
#
#    Copyright (C) 2022 Ruben Rodriguez <ruben@trisquel.info>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301 USA
#

set -ex
[ ! -d logs ] && mkdir logs
{
echo "Started at $(date +'%Y-%m-%d %H:%M:%S')" >> logs/makeall.log

if [ $UID != 0 ]; then
    echo You need to run this script as root!
    exit 1
fi

# True if $1 is greater than $2
version_gt() { dpkg --compare-versions "$1" gt "$2"; }

CODENAME=aramo
VERSION=11.0.1
ARCH_BUILD=amd64
MKTORRENT=$PWD/files/mktorrent-1.0/mktorrent
TRACKER=http://tracker.trisquel.org:6969/announce

rm NOT-FOUND source iso/* -rf

bash makeiso.sh all $ARCH_BUILD trisquel $CODENAME i18n
bash makeiso.sh all $ARCH_BUILD trisquel-mini $CODENAME
bash makeiso.sh all $ARCH_BUILD triskel $CODENAME
bash makeiso.sh all $ARCH_BUILD trisquel $CODENAME i18n fsf
bash makeiso.sh all $ARCH_BUILD trisquel-sugar $CODENAME
bash makeiso.sh source $ARCH_BUILD trisquel $CODENAME
bash makearmimage.sh $CODENAME armhf $VERSION
version_gt $VERSION 10.9 && bash makearmimage.sh $CODENAME arm64 $VERSION

cd iso
ARCHES=amd64
version_gt $VERSION 10.9 && ARCHES="amd64 ppc64el arm64"
for ARCH in $ARCHES
do
    mv trisquel-netinst_${VERSION}*$ARCH.iso trisquel-netinst_${VERSION}_$ARCH.iso
    sha256sum trisquel-netinst_${VERSION}_$ARCH.iso > trisquel-netinst_${VERSION}_$ARCH.iso.sha256
    sha512sum trisquel-netinst_${VERSION}_$ARCH.iso > trisquel-netinst_${VERSION}_$ARCH.iso.sha512
done

MIRRORS="https://cdimage.trisquel.org/trisquel-images/
https://mirror.fsf.org/trisquel-images/
https://mirror.math.princeton.edu/pub/trisquel-iso/
https://mirrors.ocf.berkeley.edu/trisquel-images/
https://ftp.acc.umu.se/mirror/trisquel/iso/
https://mirror.linux.pizza/trisquel/images/
https://ftpmirror1.infania.net/mirror/trisquel/iso/
https://mirror.librelabucm.org/trisquel-images/
https://ftp.caliu.cat/pub/distribucions/trisquel/iso/
https://quantum-mirror.hu/mirrors/pub/trisquel/iso/
https://mirror.cedia.org.ec/trisquel.iso/
https://mirrors.dotsrc.org/trisquel-iso/
https://mirrors.ustc.edu.cn/trisquel-images/
https://mirrors.nju.edu.cn/trisquel-images/
https://mirror.csclub.uwaterloo.ca/trisquel/iso/"
maketorrent(){
    FILE=$1
    DESC=$2
    SEEDS=$(for i in $MIRRORS; do
 	      echo -n ${i}$FILE','
            done | sed 's/,$//')

    $MKTORRENT -a $TRACKER -c "Trisquel GNU/Linux $VERSION $CODENAME $DESC" -w $SEEDS $FILE
}

ARCHES=amd64
version_gt $VERSION 10.9 && ARCHES="amd64 ppc64el arm64"
for ARCH in $ARCHES
do
    maketorrent trisquel-netinst_${VERSION}_$ARCH.iso "$ARCH Network Installer"
done
    maketorrent trisquel-base_${VERSION}_armhf.tar.bz2 "ARM32/armhf/ARMv7 base image"
    version_gt $VERSION 10.9 && maketorrent trisquel-base_${VERSION}_arm64.tar.bz2 "arm64/ARMv7 base image"

}  > >(tee -a logs/makeall.log) 2> >(tee -a logs/makeall.log >&2)
